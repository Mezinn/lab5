﻿using System;
using System.Windows.Forms;

namespace KPZ3
{
    public partial class Frmmain : Form
    {
        public Frmmain()
        {
            if (!string.IsNullOrEmpty(Properties.Settings.Default.Language))
            {
                System.Threading.Thread.CurrentThread.CurrentUICulture = System.Globalization.CultureInfo.GetCultureInfo(Properties.Settings.Default.Language);
                System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.GetCultureInfo(Properties.Settings.Default.Language);
            }
            InitializeComponent();
            saveToolStripMenuItem.Enabled = false;
        }

        private int openDocuments = 0;

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                blank frm = new blank();
                frm.DocName = "Untitled " + ++openDocuments;
                frm.Text = frm.DocName;
                frm.MdiParent = this;
                frm.Show();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "3", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            
        }

        private void arrangeIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.LayoutMdi(MdiLayout.ArrangeIcons);
        }

        private void cascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.LayoutMdi(MdiLayout.Cascade);
        }

        private void tileHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void tileVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.LayoutMdi(MdiLayout.TileVertical);
        }

        private void cutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                blank frm = (blank)this.ActiveMdiChild;
                frm.Cut();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "3", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                blank frm = (blank)this.ActiveMdiChild;
                frm.Copy();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "3", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            
        }

        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                blank frm = (blank)this.ActiveMdiChild;
                frm.Paste();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "3", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                blank frm = (blank)this.ActiveMdiChild;
                frm.Delete();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "3", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            
        }

        private void selectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                blank frm = (blank)this.ActiveMdiChild;
                frm.SelectAll();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "3", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    blank frm = new blank();
                    frm.Open(openFileDialog.FileName);
                    frm.MdiParent = this;
                    frm.DocName = openFileDialog.FileName;
                    frm.Text = frm.DocName;
                    frm.Show();
                    saveToolStripMenuItem.Enabled = true;
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message, "3", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                blank frm = (blank)this.ActiveMdiChild;
                frm.Save(frm.DocName);
                frm.IsSaved = true;
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "3", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            
        }

        private void saveAsStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    blank frm = (blank)this.ActiveMdiChild;
                    frm.Save(saveFileDialog.FileName);
                    frm.MdiParent = this;
                    frm.DocName = saveFileDialog.FileName;
                    frm.Text = frm.DocName;
                    saveToolStripMenuItem.Enabled = true;
                    frm.IsSaved = true;
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message, "3", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                
            }
        }

        private void fontToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                blank frm = (blank)this.ActiveMdiChild;
                frm.Font();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "3", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            
        }

        private void colorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                blank frm = (blank)this.ActiveMdiChild;
                frm.Color();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "3", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void FindStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                FindForm frm = new FindForm();
                if (frm.ShowDialog(this) == DialogResult.Cancel) return;
                blank form = (blank)this.ActiveMdiChild;
                form.MdiParent = this;
                int start = form.richTextBox.SelectionStart;
                form.richTextBox.Find(frm.FindText, start, frm.FindCondition);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "3", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            
        }

        private void aboutProgrammToolStripMenuItem_Click(object sender, EventArgs e)
        {
            About frm = new About();
            frm.ShowDialog();
        }

        private void Frmmain_Load(object sender, EventArgs e)
        {
            comboBox.DataSource = new System.Globalization.CultureInfo[] {
                System.Globalization.CultureInfo.GetCultureInfo("en-US"),
                System.Globalization.CultureInfo.GetCultureInfo("ru-RU")
            };
            comboBox.DisplayMember = "NativeName";
            comboBox.ValueMember = "Name";
            if (!string.IsNullOrEmpty(Properties.Settings.Default.Language))
            {
                comboBox.SelectedValue = Properties.Settings.Default.Language;
            }
        }

        private void Frmmain_FormClosing(object sender, FormClosingEventArgs e)
        {
            Properties.Settings.Default.Language = comboBox.SelectedValue.ToString();
            Properties.Settings.Default.Save();
        }

        private void centerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                blank frm = (blank)this.ActiveMdiChild;
                frm.AlignmentCenter();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "3", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            
        }

        private void leftToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                blank frm = (blank)this.ActiveMdiChild;
                frm.AlignmentLeft();
            }
            catch (Exception exc)
            {
                 MessageBox.Show(exc.Message, "3", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            
        }

        private void rightToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                blank frm = (blank)this.ActiveMdiChild;
                frm.AlignmentRight();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "3", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            
        }

        private void pictureToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                blank frm = (blank)this.ActiveMdiChild;
                frm.InsertingPictures();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "3", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
    }
}