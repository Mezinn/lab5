﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace KPZ3
{
    public partial class blank : Form
    {
        public string DocName = "";
        private string BufferText = "";
        public bool IsSaved = false;

        public blank()
        {
            InitializeComponent();
            sbTime.Text = Convert.ToString(DateTime.Now.ToLongTimeString());
            sbTime.ToolTipText = Convert.ToString(DateTime.Today.ToLongDateString());
        }
         
        public void Cut()
        {
            this.BufferText = richTextBox.SelectedText;
            richTextBox.SelectedText = "";
        }
        
        public void Copy()
        {
            this.BufferText = richTextBox.SelectedText;
        }
        
        public void Paste()
        {
            richTextBox.SelectedText = this.BufferText;
        }
        
        public void SelectAll()
        {
            richTextBox.SelectAll();
        }
        
        public void Delete()
        {
            richTextBox.SelectedText = "";
            this.BufferText = "";
        }

        public new void Font()
        {
            FontDialog fd = new FontDialog();
            fd.Font = richTextBox.SelectionFont;
            if (fd.ShowDialog() == DialogResult.OK)
            {
                richTextBox.SelectionFont = fd.Font;
            }
        }

        public void Color()
        {
            ColorDialog fd = new ColorDialog();
            fd.Color = richTextBox.SelectionColor;
            if (fd.ShowDialog() == DialogResult.OK)
            {
                richTextBox.SelectionColor = fd.Color;
            }
        }

        private void cutToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            Cut();
        }

        private void copyToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            Copy();
        }

        private void pasteToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            Paste();
        }

        private void deleteToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            Delete();
        }

        private void selectAllToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            SelectAll();
        }

        private void fontToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            Font();
        }

        private void colorToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            Color();
        }

        public void Open(string OpenFileName)
        {
            if (OpenFileName == "")
            {
                return;
            }
            else
            {
                StreamReader sr = new StreamReader(OpenFileName);
                richTextBox.Text = sr.ReadToEnd();
                sr.Close();
                DocName = OpenFileName;
            }
        }

        public void Save(string SaveFileName)
        {
            if (SaveFileName == "")
            {
                return;
            }
            else
            {
                StreamWriter sw = new StreamWriter(SaveFileName);
                sw.WriteLine(richTextBox.Text);
                sw.Close();
                DocName = SaveFileName;
            }
        }

        private void blank_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (IsSaved == true)
            {
                if (MessageBox.Show("Do you want save changes in " + this.DocName + "?",
                    "Message", MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    this.Save(this.DocName);
                }
            }
        }

        private void richTextBox_TextChanged(object sender, System.EventArgs e)
        {
            sbAmount.Text = "Аmount of symbols: " + richTextBox.Text.Length.ToString();
        }

        public void AlignmentCenter()
        {
            richTextBox.SelectionAlignment = HorizontalAlignment.Center;
        }

        public void AlignmentLeft()
        {
            richTextBox.SelectionAlignment = HorizontalAlignment.Left;
        }

        public void AlignmentRight()
        {
            richTextBox.SelectionAlignment = HorizontalAlignment.Right;
        }

        public void InsertingPictures()
        {
            object orgdata = Clipboard.GetDataObject();
            openFileDialogPictures.Filter = "Image Files(*.BMP;*.JPG;*.GIF)|*.BMP;*.JPG;*.GIF|All files (*.*)|*.*";
            openFileDialogPictures.FileName = "";
            openFileDialogPictures.Multiselect = false;
            if (openFileDialogPictures.ShowDialog() == DialogResult.OK)
            {
                Image img = Image.FromFile(openFileDialogPictures.FileName);
                Clipboard.SetImage(img);
                richTextBox.Paste();
            }
            Clipboard.SetDataObject(orgdata);
        }
    }
}